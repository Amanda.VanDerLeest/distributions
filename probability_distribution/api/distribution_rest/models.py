from django.db import models

# Create your models here.
class Distribution(models.Model):
    class DistributionTypes(models.TextChoices):
        NORMAL = "Normal Distribution"
        BINOMIAL = "Binomial Distribution"
        POISSON = "Poisson Distribution"
        UNIFORM = "Uniform Distribution"
        EXPONENTIAL = "Exponential Distribution"
        BERNOULLI = "Bernoulli Distribution"

    title = models.CharField(max_length=100, null=False, blank=False)
    type = models.CharField(
        max_length=30,
        choices=DistributionTypes.choices,
        default=DistributionTypes.NORMAL,
    )


class DistributionData(models.Model):
    distribution = models.ForeignKey(
        Distribution, related_name="distribution_data", on_delete=models.CASCADE
    )
    input = models.FloatField(null=False, blank=False)
