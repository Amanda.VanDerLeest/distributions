from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Distribution, DistributionData

from .encoders import (
    DistributionListEncoder,
    DistributionDetailEncoder,
    DistributionDataListEncoder,
    DistributionDataDetailEncoder,
)

# Create your views here.

# Distribution Model APIs
@require_http_methods(["GET", "POST"])
def api_distributions(request):
    if request.method == "GET":
        distributions = Distribution.objects.all()
        return JsonResponse(
            {"distributions": distributions}, encoder=DistributionListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            distribution = Distribution.objects.create(**content)
            return JsonResponse(
                distribution,
                encoder=DistributionListEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Sorry, we could not create this Distribution"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_distribution(request, pk):
    if request.method == "GET":
        distribution = Distribution.objects.get(id=pk)
        return JsonResponse(distribution, encoder=DistributionDetailEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            distribution = Distribution.objects.get(id=pk)
            distribution.delete()
            return JsonResponse(
                distribution,
                encoder=DistributionDetailEncoder,
                safe=False,
            )
        except Distribution.DoesNotExist:
            return JsonResponse(
                {"message": "Sorry, this Distribution does not exist."}, status=404
            )
    else:
        try:
            content = json.loads(request.body)
            distribution = Distribution.objects.get(id=pk)
            props = ["title", "type"]
            for prop in props:
                if prop in content:
                    setattr(distribution, prop, content[prop])
            distribution.save()
            return JsonResponse(distribution, DistributionDetailEncoder, safe=False)
        except distribution.DoesNotExist:
            return JsonResponse(
                {"message": "This distribution does not exist."},
                status=404,
            )


# DistributionData Model APIs
@require_http_methods(["GET", "POST"])
def api_distributions_data(request):
    if request.method == "GET":
        distributions_data = DistributionData.objects.all()
        return JsonResponse(
            {"distributions_data": distributions_data},
            encoder=DistributionDataListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            # this block is to access the foreignkey data
            distribution_id = content["distribution"]
            try:
                distribution = Distribution.objects.get(id=distribution_id)
                content["distribution"] = distribution
            except Distribution.DoesNotExist:
                response = JsonResponse({"message": "This distribution does not exist"})
                response.status_code = 404
                return response
            # end
            distributions_data = DistributionData.objects.create(**content)
            return JsonResponse(
                distributions_data,
                encoder=DistributionDataListEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Sorry, we could not create this Distribution Data"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_distribution_data(request, pk):
    if request.method == "GET":
        distribution_data = DistributionData.objects.get(id=pk)
        return JsonResponse(
            distribution_data, encoder=DistributionDataDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        try:
            distribution_data = DistributionData.objects.get(id=pk)
            distribution_data.delete()
            return JsonResponse(
                distribution_data,
                encoder=DistributionDataDetailEncoder,
                safe=False,
            )
        except DistributionData.DoesNotExist:
            return JsonResponse(
                {"message": "Sorry, this Distribution Data does not exist."}, status=404
            )
    else:
        try:
            content = json.loads(request.body)
            distribution_data = DistributionData.objects.get(id=pk)
            props = ["input"]
            for prop in props:
                if prop in content:
                    setattr(distribution_data, prop, content[prop])
            distribution_data.save()
            return JsonResponse(
                distribution_data, DistributionDataDetailEncoder, safe=False
            )
        except distribution_data.DoesNotExist:
            return JsonResponse(
                {"message": "This distribution data does not exist."},
                status=404,
            )
