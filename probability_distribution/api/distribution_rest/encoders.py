from common.json import ModelEncoder

from .models import Distribution, DistributionData


# Distribution Encoders


class DistributionListEncoder(ModelEncoder):
    model = Distribution
    properties = ["id", "title", "type"]


class DistributionDetailEncoder(ModelEncoder):
    model = Distribution
    properties = ["id", "title", "type"]


# DistributionData Encoder


class DistributionDataListEncoder(ModelEncoder):
    model = DistributionData
    properties = ["id", "distribution", "input"]

    encoders = {
        "distribution": DistributionListEncoder(),
    }


class DistributionDataDetailEncoder(ModelEncoder):
    model = DistributionData
    properties = ["id", "distribution", "input"]

    encoders = {
        "distribution": DistributionListEncoder(),
    }
