from django.test import TestCase
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "distribution_project.settings")
import django

django.setup()

from ..models import Distribution, DistributionData


class DistributionDataTest(TestCase):
    def setUp(self):
        distribution_score = Distribution.objects.create(
            title="Trigonometry Final Exam Scores", type="Normal Distribution"
        )
        distribution_height = Distribution.objects.create(
            title="Height of NBA Players (inches)", type="Normal Distribution"
        )
        DistributionData.objects.create(distribution=distribution_score, input=97.8)
        DistributionData.objects.create(distribution=distribution_height, input=75.75)

    def test_data_input(self):
        height_distribution = Distribution.objects.get(
            title="Height of NBA Players (inches)"
        )
        height_distribution_data = DistributionData.objects.get(
            distribution=height_distribution
        )
        score_distribution = Distribution.objects.get(
            title="Trigonometry Final Exam Scores"
        )
        score_distribution_data = DistributionData.objects.get(
            distribution=score_distribution
        )
        self.assertEqual(height_distribution_data.input, 75.75)
        self.assertEqual(score_distribution_data.input, 97.8)
