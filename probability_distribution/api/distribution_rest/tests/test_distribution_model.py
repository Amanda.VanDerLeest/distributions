from django.test import TestCase
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "distribution_project.settings")
import django

django.setup()

from ..models import Distribution


class DistributionTest(TestCase):
    def setUp(self):
        Distribution.objects.create(
            title="Height of NBA Players", type="Normal Distribution"
        )
        Distribution.objects.create(
            title="Trigonometry Final Exam Scores", type="Normal Distribution"
        )

    def test_distribution_title(self):
        height_distribution = Distribution.objects.get(title="Height of NBA Players")
        score_distribution = Distribution.objects.get(
            title="Trigonometry Final Exam Scores"
        )
        self.assertEqual(height_distribution.type, "Normal Distribution")
        self.assertEqual(score_distribution.type, "Normal Distribution")
