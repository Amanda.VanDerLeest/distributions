from django.contrib import admin
from .models import Distribution, DistributionData

# Register your models here.
@admin.register(Distribution)
class DistributionAdmin(admin.ModelAdmin):
    pass


@admin.register(DistributionData)
class DistributionDataAdmin(admin.ModelAdmin):
    pass
