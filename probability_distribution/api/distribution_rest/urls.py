from django import views
from django.urls import path
from .views import (
    api_distributions,
    api_distribution,
    api_distributions_data,
    api_distribution_data,
)

urlpatterns = [
    path("distributions/", api_distributions, name="api_distributions"),
    path("distribution/<int:pk>/", api_distribution, name="api_distribution"),
    path("distributions_data/", api_distributions_data, name="api_distributions_data"),
    path(
        "distribution_data/<int:pk>/",
        api_distribution_data,
        name="api_distribution_data",
    ),
]
