# To Run:

    docker compose build
    docker volume create beta-data
    docker compose up

    (The first build time to render on the browser can take a moment)

# To view on Browser:

    go to http://localhost:3000/distributions

# To use the application:

    1) Start at "Create a Distribution" & add a title.
    2) Go to Distributions, click the title of the distribution you created.
    3) You can then add data which will populate in the chart.
    4) You have the ability to change the title.


# Insight into planning:

![MVP Planning](./docs/wireframes/1_mvp_planning.png)
![Database Schema and API](./docs/wireframes/2_Database_schema_and_api_endpoints.png)
![Front End MVP Planning](./docs/wireframes/3_Frontend_mvp_planning.png)
![Detail View](./docs/wireframes/4_Detail_view.png)

# Notes

- I set this up as a microservice project. If I had more time, I would add a user microservice.

- I would've asked questions to the design team to verify the most important task to be solved prior to starting in order to deliver the most important aspects of the MVP prior to deadline. 

- Due to the nature of this technical challenge, and being unable to verify with a design team the desired outcome, I decided to focus on building out an evenly based full stack application to a functional, and future-scalable, point. 

    - To start, I spent time researching, making wire frames, and planning the schema.
    
    - I researched the different Probability Distribution types and hardcoded them into the database as "distribution type" option choices. I still need to set up a chart that will work with each statistical calculation assigned to their respective choice. example:
        - Normal Distribution: P(x) = 1/Sigma*sqrt(2*Pi)  *exp (-½ * ((x-μ)/(sigma))^2)

        - In the end, I ended up not using them at this time.
    
    - I researched and used Victory as my chart package, which is a library built with d3 and react.

- If I had more time, I would've set up the settings.py file to hide environment variables in a .env file

- I was able to set up unit tests for the models, but if I had more time, I would've set them up for at least the API endpoints as well.

- I would've loved to work with handwriting and delete capabilities with D3, but making sure I at least had the ability to input data-points was important for me. 

- I would've liked to allow a user to edit and delete data points (the API endpoints are already created, just not incorporated yet in the frontend)

- I created choices for the distribution type field, which ultimately I would like those somehow being utilized with graph calculations.

- The graph requires more time. I plan to debug the x & y axis "NaN" console warnings.


# Final Remarks:

I will be finishing this project outside of this coding challenge to add to my portfolio since it seems close to completion. As a stretch goal, I would like to practice with the hand-drawn tables, incorporate the delete key to delete the last line drawn, include chart curves in and add statistics for the type of distribution chart.

Overall, I had a lot of fun with this project! I did find difficulty tackling this project within a 4-hour-time-limit. I put non-coding time, design, practice and research into this application. I did have to stop at some point, so it's safe to say that the "Victory" chart was completed outside of the coding challenge as that was an all new area of learning for me.

Thank you for extending this coding challenge to me & have a great holiday week!

