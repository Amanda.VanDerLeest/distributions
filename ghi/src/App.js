import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import DistributionsList from "./distribution_components/listDistributions";
import CreateDistributionForm from "./distribution_components/addDistribution";
import DistributionEdit from "./distribution_components/editDistribution";
import DistributionDetails from "./distribution_components/viewDistribution";
import Main from "./distribution_components/distributionChart";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/distributions" element={<DistributionsList />} />
          <Route path="distribution">
            <Route path="new" element={<CreateDistributionForm />} />
            <Route path="edit/:distributionId" element={<DistributionEdit />} />
            <Route path=":distributionId" element={<DistributionDetails />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
