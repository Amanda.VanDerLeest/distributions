import { NavLink } from "react-router-dom";
import logo from "./chart3.png";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-white navbar-brand nav_line_shadow">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/distributions">
          <img
            alt="ProbabilityDistribution.com Homepage"
            className="header-logo header-logo_img"
            src={logo}
            width={110}
          />
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="/distributions"
                style={({ isActive }) => {
                  return {
                    color: isActive ? "gray" : "",
                  };
                }}
              >
                Distributions
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="/distribution/new"
                style={({ isActive }) => {
                  return {
                    color: isActive ? "gray" : "",
                  };
                }}
              >
                Create a Distribution
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
