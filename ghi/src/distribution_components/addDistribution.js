import React from "react";
import { Navigate } from "react-router-dom";

class CreateDistributionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      distributionCreated: false,
      distributionNotCreated: false,
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange(event) {
    const value = event.target.value;
    this.setState({ title: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.distributionCreated;
    delete data.distributionNotCreated;

    const distributionPostUrl = "http://localhost:8100/api/distributions/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(distributionPostUrl, fetchConfig);
    if (response.ok) {
      const newDistribution = await response.json();
      const cleared = {
        title: "",
        distributionCreated: true,
        distributionNotCreated: false,
      };
      this.setState(cleared);
    } else {
      const cleared = {
        title: "",
        distributionCreated: false,
        distributionNotCreated: true,
      };
      this.setState(cleared);
    }
  }

  render() {
    let warningMessageClasses = "alert alert-warning d-none mb-0";
    let formClasses = "";
    if (this.state.distributionCreated) {
      return <Navigate to={`/distributions`} />;
    }
    if (this.state.distributionNotCreated) {
      warningMessageClasses = "alert alert-warning mb-0";
      formClasses = "d-none";
    }

    return (
      <>
        <div className="row">
          <div className="offset-3 col-7">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Distribution</h1>
              <form
                className={formClasses}
                id="create-distribution-form"
                onSubmit={this.handleSubmit}
              >
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleTitleChange}
                    value={this.state.title}
                    placeholder="Distribution Title"
                    required
                    type="text"
                    name="title"
                    id="title"
                    className="form-control"
                  />
                  <label htmlFor="title">Distribution Title</label>
                  <div id="DistributionHelp" className="form-text">
                    Please enter your Distribution Title.
                  </div>
                </div>
                <button className="btn btn-primary">Create Distribution</button>
              </form>
              <div className={warningMessageClasses} id="warning-message">
                Uh oh! Something went wrong. Please try adding the distribution
                again.
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default CreateDistributionForm;
