import React from "react";
import { Navigate } from "react-router-dom";

class CreateDataForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distribution: props.distributionId,
      input: "",
      dataCreated: false,
      dataNotCreated: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const value = event.target.value;
    this.setState({ input: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.dataCreated;
    delete data.dataNotCreated;

    const dataPostUrl = "http://localhost:8100/api/distributions_data/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(dataPostUrl, fetchConfig);
    if (response.ok) {
      const newData = await response.json();
      const cleared = {
        input: "",
        dataCreated: true,
        dataNotCreated: false,
      };
      this.setState(cleared);
      this.props.handleDataUpdate();
    } else {
      const cleared = {
        input: "",
        dataCreated: false,
        dataNotCreated: true,
      };
      this.setState(cleared);
    }
  }

  render() {
    return (
      <>
        <div className="row">
          <div className="offset-3 col-7">
            <div className="shadow p-4 mt-4">
              <h1>New Data Point</h1>
              <form id="create-distribution-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.input}
                    placeholder="Data Point"
                    required
                    type="text"
                    name="input"
                    id="input"
                    className="form-control"
                  />
                  <label htmlFor="input">Data Point</label>
                  <div id="DataHelp" className="form-text">
                    Please enter a data point.
                  </div>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default CreateDataForm;
