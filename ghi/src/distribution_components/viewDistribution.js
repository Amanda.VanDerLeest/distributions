import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import CreateDataForm from "./addData";
import DataList from "./listData";
import Chart from "./distributionChart";

const DistributionDetails = (props) => {
  const { distributionId } = useParams();
  const BASE_DISTRIBUTION_URL = `http://localhost:8100/api/distribution`;
  const BASE_DATA_URL = `http://localhost:8100/api/distribution_data`;
  const [distributionTitle, setCurrentDistributionTitle] = useState("");
  const [distributionType, setCurrentDistributionType] = useState("");
  const [DataInput, setCurrentDataInput] = useState("");
  const [updateData, setUpdateData] = useState(false);

  useEffect(() => {
    const distributionUrl = `${BASE_DISTRIBUTION_URL}/${distributionId}/`;
    fetch(distributionUrl)
      .then((res) => res.json())
      .then((data) => {
        setCurrentDistributionTitle(data.title);
        setCurrentDistributionType(data.type);
      });
  }, [distributionId, BASE_DISTRIBUTION_URL]);

  function handleDataUpdate() {
    setUpdateData(true);
  }

  function dataUpdated() {
    setUpdateData(false);
  }

  const title = distributionTitle;
  const type = distributionType;
  return (
    <>
      <div className="py-8">
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="container">
            <div className="row">
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: "40px",
                  marginBottom: "20px",
                }}
              >
                <h1>{distributionTitle}</h1>
              </div>
            </div>
          </div>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="container">
            <div className="row">
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <button
                  className="primary-btn btn-outline-light"
                  type="button"
                  data-bs-target="#EditDistribution"
                >
                  <Link
                    to={`/distribution/edit/${distributionId}`}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    Edit Title
                  </Link>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Chart
              distributionId={distributionId}
              updateData={updateData}
              dataUpdated={dataUpdated}
            />
          </div>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div className="container">
            <div className="row">
              <div className="col-sm-6">
                <div className="">
                  <div className="card-body">
                    <CreateDataForm
                      distributionId={distributionId}
                      handleDataUpdate={handleDataUpdate}
                    />
                  </div>
                </div>
              </div>
              <div className="col-sm-5">
                <div className="">
                  <div className="card-body">
                    <DataList
                      distributionId={distributionId}
                      updateData={updateData}
                      dataUpdated={dataUpdated}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DistributionDetails;
