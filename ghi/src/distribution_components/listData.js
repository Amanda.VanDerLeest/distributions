import React from "react";
import { Link } from "react-router-dom";

function Data(props) {
  return (
    <tr>
      <td>{props.distributions_data.input}</td>
    </tr>
  );
}

class DataList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distribution: props.distributionId,
      DataColumn: [],
      updateData: props.updateData,
    };
    this.dataRefresh = this.dataRefresh.bind(this);
  }

  async dataRefresh() {
    const url = "http://localhost:8100/api/distributions_data/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const filteredData = data.distributions_data.filter(
          (inputData) => inputData.distribution.id == this.state.distribution
        );
        this.setState({ DataColumn: filteredData, updateData: false });
        this.props.dataUpdated();
      }
    } catch (e) {
      console.error(e);
    }
  }

  async componentDidMount() {
    this.dataRefresh();
  }

  componentDidUpdate(prevProps) {
    if (this.props.updateData != prevProps.updateData) {
      this.dataRefresh();
    }
  }

  render() {
    return (
      <>
        <div>
          <h1 className="display-5 fw-bold text-center">Data Points</h1>
        </div>

        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th>Input</th>
            </tr>
          </thead>
          <tbody>
            {this.state.DataColumn.map((dataList, index) => {
              return <Data key={index} distributions_data={dataList} />;
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default DataList;
