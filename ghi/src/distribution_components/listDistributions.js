import React from "react";
import { Link } from "react-router-dom";

function Distribution(props) {
  return (
    <tr>
      <td>
        <Link
          to={`/distribution/${props.distribution.id}`}
          style={{ textDecoration: "none", color: "white" }}
        >
          {props.distribution.title}
        </Link>
      </td>
    </tr>
  );
}

class DistributionsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      DistributionColumn: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/distributions/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({ DistributionColumn: data.distributions });
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div>
          <h1 className="display-5 fw-bold text-center">
            Probability Distributions
          </h1>
        </div>

        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th>Title</th>
            </tr>
          </thead>
          <tbody>
            {this.state.DistributionColumn.map((distributionsList, index) => {
              return (
                <Distribution key={index} distribution={distributionsList} />
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default DistributionsList;
