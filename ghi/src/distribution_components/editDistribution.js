import { useParams } from "react-router-dom";
import { Navigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FloatingLabel from "react-bootstrap/FloatingLabel";

const DistributionEdit = (props) => {
  const { distributionId } = useParams();
  const BASE_URL = `http://localhost:8100/api/distribution`;
  const [distributionTitle, setCurrentDistributionTitle] = useState("");
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    async function getDistributionData() {
      const url = `${BASE_URL}/${distributionId}/`;
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        const distribution = data;
        setCurrentDistributionTitle(distribution.title);
      }
    }
    if (distributionId) {
      getDistributionData();
    }
  }, [distributionId, BASE_URL]);

  function handleChangeDistributionTitle(event) {
    setCurrentDistributionTitle(event.target.value);
  }

  async function handleUpdate(event) {
    event.preventDefault();

    const data = {
      title: distributionTitle,
    };
    const url = `${BASE_URL}/${distributionId}/`;

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setSubmitted(true);
    }
  }

  if (submitted) {
    return <Navigate to={`/distribution/${distributionId}/`} />;
  }

  if (distributionId) {
    return (
      <>
        <div className="py-8">
          <div className="container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Edit your Distribution Title</h1>
                  <Form onSubmit={handleUpdate}>
                    <Form.Group
                      className="mb-3"
                      controlId="editDistributionForm"
                    >
                      <FloatingLabel
                        controlId="floatingInput"
                        label="Title"
                        className="mb-3"
                      >
                        <Form.Control
                          type="text"
                          name="Title"
                          onChange={handleChangeDistributionTitle}
                          defaultValue={distributionTitle}
                        />
                      </FloatingLabel>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                      Update
                    </Button>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
};

export default DistributionEdit;
