import React from "react";
import ReactDOM from "react-dom";
import {
  VictoryBar,
  VictoryChart,
  VictoryAxis,
  VictoryTheme,
  VictoryStack,
} from "victory";

// DEV PLANNING NOTES:

// get the data
// Grouping is going to have to order the inputs from least to greatest (sort)
// have a full dataset. Then have the groupings (each individual, no duplicates, that will be the x axis)
// grouping should count the number of each occurance
// y axis will start at 0

// const arr = ['a', 'b', 'a',
// 'a', 'c', 'c'];
// const count = {} ;
// for (const element of arr) {
// if (count[element]) {
// count [element] += 1;
// } else {
// count [element] = 1;
// }
// console. log (count);

// key:value pairs will be mapped over to fill in the x and y axis for grouping and frequency

class Chart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distribution: props.distributionId,
      rawData: [],
      dataFrequency: [],
      distributionData: [],
      tickValues: [],
      updateData: props.updateData,
    };
    this.dataRefresh = this.dataRefresh.bind(this);
    this.checkFrequency = this.checkFrequency.bind(this);
    this.setDistributionData = this.setDistributionData.bind(this);
  }

  async dataRefresh() {
    const url = "http://localhost:8100/api/distributions_data/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const filteredData = data.distributions_data
          .filter(
            (inputData) => inputData.distribution.id == this.state.distribution
          )
          .map((inputData, index) => {
            return parseFloat(inputData.input);
          });
        this.setState({ rawData: filteredData, updateData: false });
        this.props.dataUpdated();
        this.checkFrequency();
        this.setDistributionData();
      }
    } catch (e) {
      console.error(e);
    }
  }

  async componentDidMount() {
    this.dataRefresh();
  }

  componentDidUpdate(prevProps) {
    if (this.props.updateData != prevProps.updateData) {
      this.dataRefresh();
    }
  }

  checkFrequency() {
    const rawData = this.state.rawData;
    const frequency = {};
    const frequencyArray = [];
    for (const element of rawData) {
      if (frequency[element]) {
        frequency[element] += 1;
      } else {
        frequency[element] = 1;
      }
    }
    for (const element in frequency) {
      frequencyArray.push([element, frequency[element]]);
    }
    this.state.dataFrequency = frequencyArray.sort((a, b) => {
      return parseFloat(a) - parseFloat(b);
    });
  }

  setDistributionData() {
    const frequency = this.state.dataFrequency;
    const distributedData = [];
    const tickValues = [];
    for (const element of frequency) {
      distributedData.push({ grouping: element[0], frequency: element[1] });
      tickValues.push(element[0]);
    }
    this.state.distributionData = distributedData;
    this.state.tickValues = tickValues;
  }

  render() {
    return (
      <div>
        <VictoryChart domainPadding={10} theme={VictoryTheme.material}>
          <VictoryAxis tickValues={this.state.tickValues} />
          <VictoryAxis dependentAxis tickFormat={(x) => `${x / 1}`} />
          <VictoryStack colorScale={"cool"}>
            <VictoryBar
              data={this.state.distributionData}
              x={"grouping"}
              y={"frequency"}
            />
          </VictoryStack>
        </VictoryChart>
      </div>
    );
  }
}

const app = document.getElementById("app");

export default Chart;
